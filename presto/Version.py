"""
Version and authorship information
"""

__author__    = 'Jason Anthony Vander Heiden'
__copyright__ = 'Copyright 2020 Kleinstein Lab, Yale University. All rights reserved.'
__license__   = 'GNU Affero General Public License 3 (AGPL-3)'
__version__   = '0.6.2.999'
__date__      = '2021.23.08'
